import { StyleSheet, Dimensions } from "react-native";

export const colors = {
  black: "#1a1917",
  gray: "#888888",
  background1: "#1d1b25",
  background2: "#21D4FD"
};

const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
  animatedView: {
    width,
    backgroundColor: "#0a5386",
    elevation: 2,
    position: "absolute",
    bottom: 0,
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  exitTitleText: {
    textAlign: "center",
    color: "#ffffff",
    marginRight: 10
  },
  exitText: {
    color: "#e5933a",
    paddingHorizontal: 10,
    paddingVertical: 3
  },

  safeArea: {
    flex: 1,
    backgroundColor: colors.black
  },
  container: {
    flex: 1,
    backgroundColor: colors.background1
  },
  gradient: {
    ...StyleSheet.absoluteFillObject
  },
  scrollview: {
    flex: 1,
    position: "absolute",
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  exampleContainer: {
    paddingVertical: 10
  },
  exampleContainerDark: {
    backgroundColor: colors.black
  },
  exampleContainerLight: {
    backgroundColor: "white"
  },
  title: {
    paddingHorizontal: 20,
    backgroundColor: "transparent",
    color: "rgba(255, 255, 255, 0.9)",
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center"
  },
  titleDark: {
    color: colors.black,
    textAlign: "center"
  },
  subtitle: {
    marginTop: 5,
    paddingHorizontal: 20,
    backgroundColor: "transparent",
    color: "rgba(255, 255, 255, 0.75)",
    fontSize: 12,
    fontStyle: "italic",
    textAlign: "center"
  },
  slider: {
    marginTop: 2,
    overflow: "visible" // for custom animations
  },
  sliderContentContainer: {
    paddingVertical: 5 // for custom animation
  },
  paginationContainer: {
    paddingVertical: 5
  },
  paginationDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 8
  },
  container: {
    flex: 1,
    flexDirection: "column",
    height: 200
  },
  leftContainer: {
    flex: 1
    //backgroundColor: "#1b4ca6"
  },
  rightContainer: {
    flex: 1
    //  backgroundColor: "#96d0e3"
  },
  buttonContainer: {
    position: "absolute",
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  addButton: {
    zIndex: 1111,
    width: 200
  }
});
