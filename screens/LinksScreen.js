import React from "react";
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import { connect } from "react-redux";
import { logOut } from "../redux/action";
class LinksScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  logOut() {
    this.props.logOut();
    this.props.navigation.goBack();
  }

  render() {
    const loginMode = this.props.loginMode;
    const fbs = this.props.fbs;
    const googels = this.props.googels;
    console.log(googels);
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.backButton}
          onPress={() => this.logOut()}>
          <ImageBackground
            source={require("../assets/images/back.png")}
            style={styles.buttonImage}
          />
        </TouchableOpacity>
        <Text style={styles.title}>Create Account</Text>

        <View style={styles.formInput}>
          <Text style={styles.inputTitle}>Register Name</Text>
          <TextInput
            style={styles.textInput}
            placeholder="Register Name"
            value={loginMode === "FB" ? fbs.name : googels.user.name}
          />
          <View style={styles.lineStyle} />

          <Text style={styles.inputTitle}>Register Name</Text>
          <TextInput
            style={styles.textInput}
            placeholder="Email Address"
            value={loginMode === "FB" ? fbs.email : googels.user.email}
          />
          <View style={styles.lineStyle} />
        </View>
        <View style={styles.buttons}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate("Maps")}>
            <ImageBackground
              source={require("../assets/images/button-blue.png")}
              style={styles.buttonImage}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#F9FAFC"
  },
  title: {
    fontSize: 26,
    left: 20,
    top: 20,
    color: "#222F3E"
  },
  formInput: {
    marginTop: 50,
    marginLeft: 20
  },
  inputTitle: {
    color: "#576574",
    fontSize: 16
  },
  textInput: {
    color: "#576574",
    marginTop: 2,
    fontSize: 18
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: "#EEF0F4",
    marginRight: 10,
    marginTop: 5,
    marginBottom: 10
  },
  buttons: {
    bottom: 20,
    width: "100%",
    position: "absolute",
    flexDirection: "column"
  },
  button: {
    alignSelf: "center",
    height: 40,
    borderRadius: 10,
    marginVertical: 5,
    width: "90%",
    justifyContent: "center"
  },
  buttonImage: {
    width: "100%",
    height: "100%"
  },
  backButton: {
    width: 30,
    height: 25,
    marginTop: 50,
    marginLeft: 20,
    marginBottom: 10
  }
});

const mapStateToProps = state => {
  return {
    loginMode: state.myData.loginMode,
    fbs: state.myData.fbData,
    googels: state.myData.googleData
  };
};

const mapDispatchToProps = dispatch => ({
  logOut: () => dispatch(logOut())
});

LinksScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(LinksScreen);

export default LinksScreen;
