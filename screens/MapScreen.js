import React from "react";
import {
  StyleSheet,
  View,
  Animated,
  Dimensions,
  Platform,
  Image,
  Text
} from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import MapView, { Marker } from "react-native-maps";
import { withNavigation } from "react-navigation";
import SliderEntryMaps from "./styles/SliderEntryMaps";
import { connect } from "react-redux";
import { fetchMap } from "../redux/action";
const IS_IOS = Platform.OS === "ios";

const { width: viewportWidth, height: viewportHeight } = Dimensions.get(
  "window"
);

function wp(percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideHeight = viewportHeight * 0.22;
const slideWidth = wp(50);
const itemHorizontalMargin = wp(2);

const itemWidth = slideWidth + itemHorizontalMargin * 15;

const entryBorderRadius = 4;

const { width, height } = Dimensions.get("window");
const CARD_HEIGHT = slideHeight;
const CARD_WIDTH = itemWidth;
const SLIDER_1_FIRST_ITEM = 1;

class MapScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
      scrolledX: 0,
      region: {
        latitude: -7.783218,
        longitude: 110.43426,
        latitudeDelta: 0.02,
        longitudeDelta: 0.02
      }
    };
  }

  componentWillMount() {
    this.index = 0;
    this.animation = new Animated.Value(0);
    this.props.fetchMap();
  }
  componentDidMount() {
    // We should detect when scrolling has stopped then animate
    // We should just debounce the event listener here
    this.animation.addListener(({ value }) => {
      let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
      if (index >= this.props.maps.length) {
        index = this.props.maps.length - 1;
      }
      if (index <= 0) {
        index = 0;
      }
      clearTimeout(this.regionTimeout);
      this.regionTimeout = setTimeout(() => {
        if (this.index !== index) {
          this.index = index;
          this.map.animateToRegion({
            latitude: parseFloat(this.props.maps[index].lat),
            longitude: parseFloat(this.props.maps[index].lng),
            latitudeDelta: this.state.region.latitudeDelta,
            longitudeDelta: this.state.region.longitudeDelta
          });
        }
      });
    });
  }

  componentWillReceiveProps(nextProps) {
    event => {
      this.animation.setValue(event.nativeEvent.contentOffset.x);
    };
  }

  _renderItemWithParallax({ item, index }, parallaxProps) {
    return (
      <SliderEntryMaps
        data={item}
        even={(index + 1) % 2 === 0}
        parallax={true}
        parallaxProps={parallaxProps}
      />
    );
  }

  render() {
    const loginMode = this.props.loginMode;
    const fbs = this.props.fbs;
    const googels = this.props.googels;
    const { slider1ActiveSlide } = this.state;
    if (this.props.maps.length !== 0) {
      const interpolations = this.props.maps.map((marker, index) => {
        const inputRange = [
          (index - 1) * CARD_WIDTH,
          index * CARD_WIDTH,
          (index + 1) * CARD_WIDTH
        ];
        const scale = this.animation.interpolate({
          inputRange,
          outputRange: [0, 5, 0],
          extrapolate: "clamp"
        });
        const opacity = this.animation.interpolate({
          inputRange,
          outputRange: [0, 1, 0],
          extrapolate: "clamp"
        });

        return {
          scale,
          opacity
        };
      });
      const region = {
        latitude: parseFloat(this.props.maps[0].lat),
        longitude: parseFloat(this.props.maps[0].lng),
        latitudeDelta: 0.06,
        longitudeDelta: 0.06
      };
      return (
        <View style={styles.container}>
          <View
            style={{
              height: 150,
              backgroundColor: "#3742FA"
            }}>
            <View style={styles.navbarContainer}>
              <Image
                resizeMode="cover"
                source={{
                  uri:
                    loginMode == "FB"
                      ? `http://graph.facebook.com/${fbs.id}/picture?type=large`
                      : googels.user.photoUrl
                }}
                style={styles.avatarStyle}
              />

              <View style={{ flexDirection: "column", marginLeft: 10 }}>
                <Text style={styles.textUsername}>
                  {loginMode === "FB" ? fbs.name : googels.user.name}
                </Text>
                <Text style={styles.textUsername}>
                  {loginMode === "FB" ? fbs.email : googels.user.email}
                </Text>
              </View>
              <Image
                source={require("../assets/images/icon-burger-menu.png")}
                style={styles.iconBurgerM}
              />
            </View>
          </View>
          <MapView
            ref={map => (this.map = map)}
            initialRegion={region}
            style={styles.container}>
            {this.props.maps.map((marker, index) => {
              const coordinate = {
                latitude: parseFloat(marker.lat),
                longitude: parseFloat(marker.lng)
              };
              const scaleStyle = {
                transform: [
                  {
                    scale: interpolations[index].scale
                  }
                ]
              };
              const opacityStyle = {
                opacity: interpolations[index].opacity
              };

              return (
                <MapView.Marker
                  key={index}
                  coordinate={coordinate}
                  image={require("../assets/images/icon-car-parking-available.png")}>
                  <Animated.View style={[styles.markerWrap, opacityStyle]}>
                    <Animated.View style={[styles.ring, scaleStyle]}>
                      <Image
                        resizeMode="center"
                        source={require("../assets/images/icon-car-parking.png")}
                        style={{
                          width: "100%",
                          height: "70%"
                        }}
                      />
                    </Animated.View>
                  </Animated.View>
                </MapView.Marker>
              );
            })}
          </MapView>
          <Animated.View
            style={styles.scrollView}
            contentContainerStyle={styles.endPadding}>
            <Carousel
              style={styles.carouselStyle}
              ref={c => {
                this._carousel = c;
              }}
              data={this.props.maps}
              renderItem={this._renderItemWithParallax.bind(this)}
              sliderWidth={width}
              paddingHorizontal={itemHorizontalMargin}
              itemWidth={itemWidth}
              hasParallaxImages={true}
              onScroll={event => {
                this.animation.setValue(event.nativeEvent.contentOffset.x);
              }}
              onSnapToItem={index =>
                this.setState({ slider1ActiveSlide: index })
              }
              useScrollView={true}
            />
            {this.props.maps.length <= 0 ? (
              <Spinner />
            ) : (
              <Pagination
                dotsLength={this.props.maps.length}
                activeDotIndex={slider1ActiveSlide}
                containerStyle={styles.paginationContainer}
                dotColor={"#1b4ca6"}
                dotStyle={styles.paginationDot}
                inactiveDotColor={"gray"}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
                carouselRef={this._slider1Ref}
                tappableDots={!!this._slider1Ref}
              />
            )}
          </Animated.View>
        </View>
      );
    } else {
      return <View style={styles.container}>{/* <Spiner /> */}</View>;
    }
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  scrollView: {
    position: "absolute",
    bottom: 5,
    left: 0,
    right: 0,
    paddingVertical: 10
  },
  carouselStyle: {
    padding: 25
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH
  },
  card: {
    padding: 10,
    elevation: 2,
    backgroundColor: "rgba(244,255,244, 1)",
    marginHorizontal: 10,
    margin: 10,
    shadowColor: "rgba(0,72,51, 0.9)",
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 0, y: 0 },
    height: CARD_HEIGHT,
    width: CARD_WIDTH
  },
  cardImage: {
    flex: 3,
    width: "100%",
    height: "100%",
    alignSelf: "center"
  },
  textContent: {
    flex: 1
  },
  cardtitle: {
    fontSize: 12,
    marginTop: 5,
    fontWeight: "bold"
  },
  cardDescription: {
    fontSize: 12,
    color: "#444"
  },
  markerWrap: {
    alignItems: "center",
    justifyContent: "center",
    height: 32,
    width: 16
  },
  marker: {
    width: 35,
    height: 25
  },
  ring: {
    width: 7,
    height: 7,
    alignSelf: "center"
  },
  avatarStyle: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  navbarContainer: {
    marginTop: 50,
    marginTop: "auto",
    marginBottom: 20,
    marginLeft: 10,
    flexDirection: "row"
  },
  textUsername: {
    color: "white",
    fontSize: 16
  },
  iconBurgerM: {
    width: 25,
    height: 25,
    marginLeft: "auto",
    marginRight: 10,
    marginTop: 5
  }
});

const mapStateToProps = state => {
  return {
    maps: state.myData.mapData,
    loginMode: state.myData.loginMode,
    fbs: state.myData.fbData,
    googels: state.myData.googleData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchMap: () => dispatch(fetchMap())
  };
};

MapScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(MapScreen);

export default withNavigation(MapScreen);
