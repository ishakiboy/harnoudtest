import React from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  ImageBackground
} from "react-native";
import { connect } from "react-redux";
import { loginFb, loginGoogle } from "../redux/action";

class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.loginState === true) {
      this.props.navigation.navigate("Links");
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          resizeMode="cover"
          source={require("../assets/images/hero.png")}
          style={{ width: "100%", height: "100%" }}>
          <Text style={styles.title}>Alpha Parking</Text>

          <View style={styles.buttons}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.props.loginFb()}>
              <ImageBackground
                source={require("../assets/images/button-fb.png")}
                style={styles.buttonImage}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.button}
              onPress={() => this.props.loginGoogle()}>
              <ImageBackground
                source={require("../assets/images/button-google.png")}
                style={styles.buttonImage}
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.button}>
              <ImageBackground
                source={require("../assets/images/button-hp.png")}
                style={styles.buttonImage}
              />
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center"
  },
  buttons: {
    bottom: 20,
    width: "100%",
    position: "absolute",
    flexDirection: "column"
  },
  button: {
    alignSelf: "center",
    height: 40,
    borderRadius: 10,
    marginVertical: 5,
    width: "80%",
    justifyContent: "center"
  },
  buttonImage: {
    width: "100%",
    height: "100%"
  },
  title: {
    fontSize: 22,
    color: "white",
    textAlign: "center",
    top: 70,
    width: "100%"
  }
});

const mapStateToProps = state => {
  return {
    loginState: state.myData.isLogin
  };
};

const mapDispatchToProps = dispatch => ({
  loginFb: () => dispatch(loginFb()),
  loginGoogle: () => dispatch(loginGoogle())
});

LoginScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen);

export default LoginScreen;
