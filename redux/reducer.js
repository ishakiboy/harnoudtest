const intialState = {
  fbData: null,
  googleData: null,
  loginMode: null,
  isLogin: false,
  mapData: []
};

export default (myReducer = (state = intialState, action) => {
  switch (action.type) {
    default:
      return state;

    case "FB_LOGIN":
      return {
        ...state,
        fbData: action.payload.fbs,
        isLogin: action.payload.loginState,
        loginMode: "FB"
      };
    case "GOOGLE_LOGIN":
      return {
        ...state,
        googleData: action.payload.googels,
        isLogin: action.payload.loginState,
        loginMode: "GOOGLE"
      };

    case "LOGOUT":
      return {
        ...state,
        isLogin: false
      };

    case "GET_MAP":
      return {
        ...state,
        mapData: action.payload.maps
      };
  }
});
