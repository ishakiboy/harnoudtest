import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import myReducer from "./reducer";

const reducers = combineReducers({
  myData: myReducer
});

const middleware = applyMiddleware(thunk);

export default createStore(reducers, middleware);
