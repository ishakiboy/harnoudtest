import axios from "axios";
import { Facebook, Google } from "expo";
//FB : 577032659441125
//Google :145028412889-61mt02tlpegkirrs7sqrdke51c5n2q2o.apps.googleusercontent.com

export const loginFb = () => async dispatch => {
  try {
    const { type, token } = await Facebook.logInWithReadPermissionsAsync(
      "577032659441125",
      {
        permissions: ["public_profile", "email"]
      }
    );
    if (type === "success") {
      const response = await fetch(
        `https://graph.facebook.com/me?access_token=${token}`
      );

      const datafb = await response.json();

      const detailresponse = await fetch(
        `https://graph.facebook.com/${
          datafb.id
        }?fields=id,name,email&access_token=${token}`
      );
      dispatch({
        type: "FB_LOGIN",
        payload: {
          fbs: await detailresponse.json(),
          loginState: true
        }
      });
    } else {
      dispatch({
        type: "FB_LOGIN",
        payload: {
          loginState: false
        }
      });
    }
  } catch ({ message }) {
    alert(`Facebook Login Error: ${message}`);
    dispatch({
      type: "FB_LOGIN",
      payload: {
        loginState: false
      }
    });
  }
};

export const loginGoogle = () => async dispatch => {
  try {
    const result = await Google.logInAsync({
      androidClientId:
        "145028412889-61mt02tlpegkirrs7sqrdke51c5n2q2o.apps.googleusercontent.com",
      scopes: ["profile", "email"]
    });

    if (result.type === "success") {
      dispatch({
        type: "GOOGLE_LOGIN",
        payload: {
          googels: await result,
          loginState: true
        }
      });
    } else {
      dispatch({
        type: "GOOGLE_LOGIN",
        payload: {
          loginState: false
        }
      });
      return { cancelled: true };
    }
  } catch (e) {
    dispatch({
      type: "GOOGLE_LOGIN",
      payload: {
        loginState: false
      }
    });
    return { error: true };
  }
};

export const logOut = () => {
  return {
    type: "LOGOUT",
    payload: {
      logOut: false
    }
  };
};

export const fetchMap = () => {
  return dispatch => {
    axios.get("http://demo7990087.mockable.io/test").then(response => {
      const maps = response.data;
      dispatch({
        type: "GET_MAP",
        payload: {
          maps: maps.data
        }
      });
    });
  };
};
