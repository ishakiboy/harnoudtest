import React from "react";
import LoginScreen from "../screens/LoginScreen";
import LinksScreen from "../screens/LinksScreen";
import MapScreen from "../screens/MapScreen";
import { createStackNavigator } from "react-navigation";

export default createStackNavigator({
  Home: LoginScreen,
  Links: LinksScreen,
  Maps: MapScreen
});
